# Back End Assessment

Please read this document to running the project


## Prerequisites

- Python3 (>= v3.10, 3.10.7 is recomended)
- Database: Mysql(Recomended)
- Install virtual environtment (optional)
  - https://pipenv.pypa.io/en/latest/
  - https://virtualenv.pypa.io/en/latest/


## Installing & Running

After prerequisites has been setup. You have to make the program run.
  - Install dependency library for this project with run this command `pip install -r requirements.txt`
  - Make sure all variables value in your `.env` is yours
  - Run the migrations `flask db upgrade`
  - Run the seeder `flask seed run` or import from db file in /backup directory (Backuped from Dbeaver 23.0.5)

  Run The Celery Worker, Celery Beat, and Flask Server
  - Run the celery worker `celery -A app.celery worker --loglevel=info -P solo`
  - Run the celery beat `celery -A app.celery beat --loglevel=info`
  - Run this command to start server `flask run --reload` <br>
  NOTE: celery worker, celery beat, and flask server are in different services

## Test Running
 - Make sure celery worker and beat are running
 - Run this command to start test `pytest` 


## Endpoint List
 - {HOST_URL}
 - {HOST_URL}/save_emails


## REQUEST SAMPLES

- Save Email: {HOST_URL}/save_emails<br>
{<br>
    "event_id": 1,<br>
    "email_subject": "Say Hello",<br>
    "email_content": "Lorem ipsum dolor sit amet.",<br>
    "timestamp": "15 Dec 2015 23:12"<br>
}


## RUNNING GUI BE LIKE:
https://gitlab.com/cakraa3/backend-test-with-flask/-/wikis/Documentation-Image
