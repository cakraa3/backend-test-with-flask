from flask_seeder import Seeder, Faker, generator
from app.database.models import User, Event, EventMember
from sqlalchemy import insert
import json

dummy_file = open('seeds/dummy.json')
dummy_data = json.load(dummy_file)

class DummySeeder(Seeder):
  def __init__(self, db=None):
    super().__init__(db=db)
    self.priority = 1

  def create_user(self):
    self.db.session.execute(
      insert(User),
      dummy_data['users']
    )

  def create_event(self):
    self.db.session.execute(
      insert(Event),
      dummy_data['events']
    )

  def create_event_members(self):
    self.db.session.execute(
      insert(EventMember),
      dummy_data['event_members']
    )

  def run(self):
    self.create_user()
    self.create_event()
    self.create_event_members()
