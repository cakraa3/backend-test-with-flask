from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_mixins import AllFeaturesMixin


# Database ORM Configuration
db = SQLAlchemy()

# Database Migrations Configuration
migration = Migrate(directory='./app/database/migrations')

# Initialize Base Model
class BaseModel(db.Model, AllFeaturesMixin):
    __abstract__ = True
    pass
