from sqlalchemy import Column, Integer, String, Text, DateTime, Date, Float, ForeignKey
from sqlalchemy.orm import relationship, Mapped
from app.database import BaseModel
from sqlalchemy.sql import func

class EventMember(BaseModel):
    __tablename__ = 'event_members'
    id = Column(Integer, primary_key=True, autoincrement=True)
    event_id = Column(Integer, ForeignKey('events.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    created_at = Column(DateTime(timezone=True), server_default=func.now())

    user: Mapped["User"] = relationship(back_populates="event_members")