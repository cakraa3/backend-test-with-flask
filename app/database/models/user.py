from sqlalchemy import Column, Integer, String, Text, DateTime, Boolean, Float, ForeignKey
from typing import List
from sqlalchemy.orm import relationship, Mapped
from app.database import BaseModel
from sqlalchemy.sql import func

class User(BaseModel):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column(String(100))
    last_name = Column(String(100))
    date_of_birth = Column(DateTime)
    email = Column(String(100), unique=True)
    address = Column(Text)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

    event_members: Mapped[List["EventMember"]] = relationship(back_populates="user")
