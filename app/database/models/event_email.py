from sqlalchemy import Column, Integer, String, Text, DateTime, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from app.database import BaseModel
from sqlalchemy.sql import func

class EventEmail(BaseModel):
    __tablename__ = 'event_emails'
    id = Column(Integer, primary_key=True, autoincrement=True)
    subject = Column(String(255))
    content = Column(Text)
    timestamp = Column(DateTime)
    event_id = Column(Integer, ForeignKey('events.id'))
    is_sent = Column(Boolean)
    created_at = Column(DateTime(timezone=True), server_default=func.now())

