import os
from flask import Flask
from app import core
from app.database import models, db, migration
from app.routes import api
from app.core.extentions import mail
from app.utils.celery import make_celery
from app.worker import *
from seeds import seeder
from dotenv import load_dotenv

load_dotenv()

def create_app(database_uri=None):
    # Flask Initialization
    app = Flask(__name__)
    app.config.from_object(core.config.settings[os.environ.get('FLASK_ENV', 'default')])
    if database_uri:
        app.config["SQLALCHEMY_DATABASE_URI"] = database_uri

    # Database ORM Initialization
    db.init_app(app)
    migration.init_app(app, db)

    # Celery Initialization
    celery = make_celery(app)
    celery.set_default()

    # # Flask API Initialization
    api.init_app(app)

    # # Flask Mail Initialization
    mail.init_app(app)

    # # Flask Seeder Initialization
    seeder.init_app(app, db)

    return app, celery

app, celery = create_app()
app.app_context().push()