from flask_restx import Resource, Namespace, abort
from app.routes.event_members import schema
from app.database.models.event_member import EventMember
from app.database import db
import os

# Namespace of endpoints
ns = Namespace('event_members')

# Pagination Parser
pagination_parser = ns.parser()
pagination_parser.add_argument('page', type=int)
pagination_parser.add_argument('per_page', type=int)

@ns.route('')
class eventMemberResource(Resource):
    @ns.doc(parser=pagination_parser)
    @ns.marshal_with(schema.eventMemberResponseSchema, mask=None)
    def get(self):
        pagination_args = pagination_parser.parse_args()
        page = (
            pagination_args.get('page')
            if pagination_args.get('page') and pagination_args.get('page') >= 0
            else 1
            )
        per_page = (
            pagination_args.get('per_page')
            if pagination_args.get('per_page') and pagination_args.get('per_page') >= 0
            else int(os.environ.get('API_PAGINATION_PER_PAGE', 10))
            )

        event_member = EventMember.query
        event_total = event_member.count()

        if per_page:
            event_member = event_member.limit(per_page)
        if page:
            offset = per_page*(page-1)
            event_member = event_member.offset(offset)

        event_count = event_member.count()
        pagination = {
            "page": page,
            "per_page": per_page,
            "records": event_count,
            "total_records": event_total,
        }
        return  {'data': event_member.all(), 'message': 'ok', 'code': 200, 'pagination': pagination}, 200

    @ns.expect(schema.eventMemberCreateSchema, validate=True)
    @ns.marshal_with(schema.eventMemberResponseSchema, mask=None)
    def post(self):
        args = ns.payload

        # Mapping request body to model
        event_member = EventMember(**args)

        # Store data to database
        db.session.add(event_member)
        db.session.commit()
        db.session.refresh(event_member)

        return  {'data': event_member, 'message': 'EventMember created successfully.', 'code': 201}, 201

@ns.route('/<int:id>')
class eventMemberResourceDetail(Resource):
    @ns.marshal_with(schema.eventMemberResponseSchema, mask=None)
    def get(self, id):
        event_member = EventMember.query.filter(EventMember.id==id).first()
        if not event_member:
            raise abort(400, 'Id not found!', status_code=400)
        
        return  {'data': event_member, 'message': 'ok', 'code': 200}, 200

    @ns.expect(schema.eventMemberUpdateSchema, validate=True)
    @ns.marshal_with(schema.eventMemberResponseSchema, mask=None)
    def put(self, id):
        args = ns.payload

        # Check id exists
        event_member = EventMember.query.filter(EventMember.id==id).first()
        if not event_member:
            raise abort(400, 'Id not found!', status_code=400)

        # Mapping request body to model
        event_member.title = args['title']
        event_member.content = args['content']
        event_member.date_start = args['date_start']
        event_member.date_end = args['date_end']
        event_member.location = args['location']
        event_member.ticket_fee = args['ticket_fee']

        # Update data to database
        db.session.commit()
        db.session.refresh(event_member)

        return  {'data': event_member, 'message': 'EventMember updated successfully.', 'code': 200}, 200
