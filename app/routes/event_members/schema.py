from app.routes.api import api
from flask_restx import fields


eventMemberSchema = api.model('eventMemberSchema', {
    'id': fields.Integer,
    'event_id': fields.Integer,
    'user_id': fields.Integer,
    'created_at': fields.DateTime,
})

eventMemberCreateSchema = api.model('eventMemberCreateSchema', {
    'id': fields.Integer(required=True),
    'event_id': fields.Integer(required=True),
    'user_id': fields.Integer(required=True),
})

eventMemberUpdateSchema = api.model('eventMemberUpdateSchema', {
    'id': fields.Integer,
    'event_id': fields.Integer,
    'user_id': fields.Integer,
})

eventMemberResponseSchema = api.model('eventMemberResponseSchema', {
    'data': fields.Nested(eventMemberSchema),
    'message': fields.String,
    'code': fields.Integer,
})

paginationSchema = api.model('paginationSchema', {
    "page": fields.Integer,
    "per_page": fields.Integer,
    "records": fields.Integer,
    "total_records": fields.Integer,
    "total_pages": fields.Integer,
})

eventMemberResponseSchema = api.model('eventMemberResponseListSchema', {
    'data': fields.Nested(eventMemberSchema),
    'message': fields.String,
    'code': fields.Integer,
    "pagination": fields.Nested(paginationSchema),
})