from flask_restx import Resource, Namespace, abort
from app.routes.save_emails.schema import eventEmailCreateSchema, eventEmailResponseSchema
from app.database.models.event import Event
from app.database.models.event_email import EventEmail
from app.database import db
import email.utils


# Namespace of endpoints
ns = Namespace('save_emails')


@ns.route('')
class SaveEmail(Resource):
    @ns.expect(eventEmailCreateSchema, validate=True)
    @ns.marshal_with(eventEmailResponseSchema, mask=None)
    def post(self):
        args = ns.payload
        timestamp = email.utils.parsedate_to_datetime(args['timestamp'])

        # Event Validation
        event = Event.query.filter(Event.id==args['event_id']).first()
        if not event:
            raise abort(400, 'Event id not found!', status_code=400)

        # Mapping request body to model
        eventEmail = EventEmail(
            subject = args['email_subject'],
            content = args['email_content'],
            timestamp = timestamp,
            event_id = args['event_id'],
        )

        # Store data to database
        db.session.add(eventEmail)
        db.session.commit()
        db.session.refresh(eventEmail)

        return  {'data': eventEmail, 'message': 'Email created successfully.', 'code': 201}, 201
