from app.routes.api import api
from flask_restx import fields


eventEmailSchema = api.model('EventEmail', {
    'id': fields.Integer,
    'subject': fields.String,
    'content': fields.String,
    'timestamp': fields.DateTime(dt_format='rfc822'),
    'event_id': fields.Integer,
    'is_sent': fields.Boolean,
    'created_at': fields.DateTime()
})

eventEmailCreateSchema = api.model('EventEmail', {
    'event_id': fields.Integer(required=True),
    'email_subject': fields.String(required=True),
    'email_content': fields.String(required=True),
    'timestamp': fields.DateTime(dt_format='rfc822', required=True, default="15 Dec 2015 23:12"),
})

eventEmailResponseSchema = api.model('response', {
    'data': fields.Nested(eventEmailSchema),
    'message': fields.String,
    'code': fields.Integer
})

