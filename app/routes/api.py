# Flask RestX API
from flask_restx import Api
api = Api(
    version="1.0",
    title="REST API",
    description="REST API"
)

# Adding Namespace Object to API
from app.routes.save_emails.resources import ns as ns_save_email
from app.routes.users.resources import ns as ns_user
from app.routes.events.resources import ns as ns_event
from app.routes.event_members.resources import ns as ns_event_member

api.add_namespace(ns_save_email)
api.add_namespace(ns_user)
api.add_namespace(ns_event)
api.add_namespace(ns_event_member)
