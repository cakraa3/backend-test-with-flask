from app.routes.api import api
from flask_restx import fields


userSchema = api.model('userSchema', {
    'id': fields.Integer,
    'first_name': fields.String,
    'last_name': fields.String,
    'date_of_birth': fields.DateTime,
    'email': fields.String,
    'address': fields.String,
    'created_at': fields.DateTime,
    'updated_at': fields.DateTime,
})

userCreateSchema = api.model('userCreateSchema', {
    'first_name': fields.String(required=True),
    'last_name': fields.String(),
    'date_of_birth': fields.DateTime(required=True),
    'email': fields.String(required=True),
    'address': fields.String(required=True),
})

userUpdateSchema = api.model('userUpdateSchema', {
    'first_name': fields.String,
    'last_name': fields.String,
    'date_of_birth': fields.DateTime,
    'email': fields.String,
    'address': fields.String,
})

userResponseSchema = api.model('userResponseSchema', {
    'data': fields.Nested(userSchema),
    'message': fields.String,
    'code': fields.Integer
})

paginationSchema = api.model('paginationSchema', {
    "page": fields.Integer,
    "per_page": fields.Integer,
    "records": fields.Integer,
    "total_records": fields.Integer,
    "total_pages": fields.Integer,
})

userResponseSchema = api.model('userResponseListSchema', {
    'data': fields.Nested(userSchema),
    'message': fields.String,
    'code': fields.Integer,
    "pagination": fields.Nested(paginationSchema),
})