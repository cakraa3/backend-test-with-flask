from flask_restx import Resource, Namespace, abort
from app.routes.users import schema
from app.database.models.user import User
from app.database import db
import os

# Namespace of endpoints
ns = Namespace('users')

# Pagination Parser
pagination_parser = ns.parser()
pagination_parser.add_argument('page', type=int)
pagination_parser.add_argument('per_page', type=int)

@ns.route('')
class UserResource(Resource):
    @ns.doc(parser=pagination_parser)
    @ns.marshal_with(schema.userResponseSchema, mask=None)
    def get(self):
        pagination_args = pagination_parser.parse_args()
        page = (
            pagination_args.get('page')
            if pagination_args.get('page') and pagination_args.get('page') >= 0
            else 1
            )
        per_page = (
            pagination_args.get('per_page')
            if pagination_args.get('per_page') and pagination_args.get('per_page') >= 0
            else int(os.environ.get('API_PAGINATION_PER_PAGE', 10))
            )

        user = User.query
        user_total = user.count()

        if per_page:
            user = user.limit(per_page)
        if page:
            offset = per_page*(page-1)
            user = user.offset(offset)

        user_count = user.count()
        pagination = {
            "page": page,
            "per_page": per_page,
            "records": user_count,
            "total_records": user_total,
        }
        return  {'data': user.all(), 'message': 'ok', 'code': 200, 'pagination': pagination}, 200

    @ns.expect(schema.userCreateSchema, validate=True)
    @ns.marshal_with(schema.userResponseSchema, mask=None)
    def post(self):
        args = ns.payload

        # Mapping request body to model
        user = User(**args)

        # Store data to database
        db.session.add(user)
        db.session.commit()
        db.session.refresh(user)

        return  {'data': user, 'message': 'User created successfully.', 'code': 201}, 201

@ns.route('/<int:id>')
class UserResourceDetail(Resource):
    @ns.marshal_with(schema.userResponseSchema, mask=None)
    def get(self, id):
        user = User.query.filter(User.id==id).first()
        if not user:
            raise abort(400, 'Id not found!', status_code=400)
        
        return  {'data': user, 'message': 'ok', 'code': 200}, 200

    @ns.expect(schema.userUpdateSchema, validate=True)
    @ns.marshal_with(schema.userResponseSchema, mask=None)
    def put(self, id):
        args = ns.payload

        # Check id exists
        user = User.query.filter(User.id==id).first()
        if not user:
            raise abort(400, 'Id not found!', status_code=400)

        # Mapping request body to model
        user.title = args['title']
        user.content = args['content']
        user.date_start = args['date_start']
        user.date_end = args['date_end']
        user.location = args['location']
        user.ticket_fee = args['ticket_fee']

        # Update data to database
        db.session.commit()
        db.session.refresh(user)

        return  {'data': user, 'message': 'User updated successfully.', 'code': 200}, 200
