from flask_restx import Resource, Namespace, abort
from app.routes.events import schema
from app.database.models.event import Event
from app.database import db
import os

# Namespace of endpoints
ns = Namespace('events')

# Pagination Parser
pagination_parser = ns.parser()
pagination_parser.add_argument('page', type=int)
pagination_parser.add_argument('per_page', type=int)

@ns.route('')
class EventResource(Resource):
    @ns.doc(parser=pagination_parser)
    @ns.marshal_with(schema.eventResponseSchema, mask=None)
    def get(self):
        pagination_args = pagination_parser.parse_args()
        page = (
            pagination_args.get('page')
            if pagination_args.get('page') and pagination_args.get('page') >= 0
            else 1
            )
        per_page = (
            pagination_args.get('per_page')
            if pagination_args.get('per_page') and pagination_args.get('per_page') >= 0
            else int(os.environ.get('API_PAGINATION_PER_PAGE', 10))
            )

        event = Event.query
        event_total = event.count()

        if per_page:
            event = event.limit(per_page)
        if page:
            offset = per_page*(page-1)
            event = event.offset(offset)

        event_count = event.count()
        pagination = {
            "page": page,
            "per_page": per_page,
            "records": event_count,
            "total_records": event_total,
        }
        return  {'data': event.all(), 'message': 'ok', 'code': 200, 'pagination': pagination}, 200

    @ns.expect(schema.eventCreateSchema, validate=True)
    @ns.marshal_with(schema.eventResponseSchema, mask=None)
    def post(self):
        args = ns.payload

        # Mapping request body to model
        event = Event(**args)

        # Store data to database
        db.session.add(event)
        db.session.commit()
        db.session.refresh(event)

        return  {'data': event, 'message': 'Event created successfully.', 'code': 201}, 201

@ns.route('/<int:id>')
class EventResourceDetail(Resource):
    @ns.marshal_with(schema.eventResponseSchema, mask=None)
    def get(self, id):
        event = Event.query.filter(Event.id==id).first()
        if not event:
            raise abort(400, 'Id not found!', status_code=400)
        
        return  {'data': event, 'message': 'ok', 'code': 200}, 200

    @ns.expect(schema.eventUpdateSchema, validate=True)
    @ns.marshal_with(schema.eventResponseSchema, mask=None)
    def put(self, id):
        args = ns.payload

        # Check id exists
        event = Event.query.filter(Event.id==id).first()
        if not event:
            raise abort(400, 'Id not found!', status_code=400)

        # Mapping request body to model
        event.title = args['title']
        event.content = args['content']
        event.date_start = args['date_start']
        event.date_end = args['date_end']
        event.location = args['location']
        event.ticket_fee = args['ticket_fee']

        # Update data to database
        db.session.commit()
        db.session.refresh(event)

        return  {'data': event, 'message': 'Event updated successfully.', 'code': 200}, 200
