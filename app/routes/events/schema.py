from app.routes.api import api
from flask_restx import fields


eventSchema = api.model('eventSchema', {
    'id': fields.Integer,
    'title': fields.String,
    'content': fields.String,
    'date_start': fields.Date,
    'date_end': fields.Date,
    'location': fields.String,
    'ticket_fee': fields.Float,
    'created_at': fields.DateTime,
    'updated_at': fields.DateTime,
})

eventCreateSchema = api.model('eventCreateSchema', {
    'title': fields.String(required=True),
    'content': fields.String(required=True),
    'date_start': fields.Date(required=True),
    'date_end': fields.Date(required=True),
    'location': fields.String(required=True),
    'ticket_fee': fields.Float(required=True),
})

eventUpdateSchema = api.model('eventUpdateSchema', {
    'title': fields.String,
    'content': fields.String,
    'date_start': fields.Date,
    'date_end': fields.Date,
    'location': fields.String,
    'ticket_fee': fields.Float,
})

eventResponseSchema = api.model('eventResponseSchema', {
    'data': fields.Nested(eventSchema),
    'message': fields.String,
    'code': fields.Integer
})

paginationSchema = api.model('paginationSchema', {
    "page": fields.Integer,
    "per_page": fields.Integer,
    "records": fields.Integer,
    "total_records": fields.Integer,
    "total_pages": fields.Integer,
})

eventResponseSchema = api.model('eventResponseListSchema', {
    'data': fields.Nested(eventSchema),
    'message': fields.String,
    'code': fields.Integer,
    "pagination": fields.Nested(paginationSchema),
})