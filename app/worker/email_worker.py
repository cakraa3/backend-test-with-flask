from celery import shared_task
from app.core.extentions import mail, Message
from app.database.models.event_email import EventEmail
from app.database.models.user import User
from app.database import db
from datetime import datetime
import os
from dotenv import load_dotenv
import pytz


load_dotenv()

sender = os.environ.get('MAIL_SENDER', 'cakraa3@gmail.com')
tz = pytz.timezone(os.environ.get('CELERY_EVENT_EMAIL_TIMEZONE', 'Asia/Singapore'))

@shared_task
def send_event_email(recipients=[], subject='', body='', event_id=None):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = body
    mail.send(msg)
    return f"Event id {event_id} email sent successfully!"

@shared_task
def check_event_email_constantly(): #Run every 10 seconds, you can change the schedule time in config

    # Check event email
    eventEmails = EventEmail.query.filter(EventEmail.is_sent.isnot(True)).order_by(EventEmail.timestamp.asc()).all()
    for eventEmail in eventEmails:
        if eventEmail and pytz.utc.localize(eventEmail.timestamp) <= datetime.now(tz=tz):

            # Get user from event email
            users = User.query.filter(User.event_members.any(event_id=eventEmail.event_id)).all()
            # Add send event email to queue
            for user in users:
                send_event_email.delay(
                    recipients=[user.email],
                    subject=eventEmail.subject,
                    body=eventEmail.content,
                    event_id=eventEmail.id
                )
            # Add update status of event email
            eventEmail.is_sent = True
            db.session.commit()
