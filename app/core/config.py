import os
from dotenv import load_dotenv
# from celery.schedules import crontab


load_dotenv()

class Config:

    # project root directory
    BASE_DIR = os.path.join(os.pardir, os.path.dirname(__file__))
    SECRET_KEY = os.environ.get("SECRET_KEY")

    # flask configuration
    # --------------------------------------------------------------------
    DEBUG = False
    TESTING = False

    # sqlalchemy database configuration
    # --------------------------------------------------------------------
    SQLALCHEMY_DATABASE_URI = "{}://{}:{}@{}:{}/{}".format(
        os.environ.get('DB_SCHEMA'),
        os.environ.get('DB_USERNAME'),
        os.environ.get('DB_PASSWORD'),
        os.environ.get('DB_HOST'),
        os.environ.get('DB_PORT'),
        os.environ.get('DB_NAME'),
    )

    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # SMTP server main configuration
    # --------------------------------------------------------------------
    MAIL_SERVER=os.environ.get('MAIL_SERVER')
    MAIL_PORT=os.environ.get('MAIL_PORT')
    MAIL_USERNAME=os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD=os.environ.get('MAIL_PASSWORD')
    MAIL_USE_TLS=eval(os.environ.get('MAIL_USE_TLS', 'True'))
    MAIL_USE_SSL=eval(os.environ.get('MAIL_USE_SSL', 'False'))

    # celery configuration
    # --------------------------------------------------------------------
    CELERY_CONFIG={
        "broker_url": os.environ.get('BROKER_URL'),
        "result_backend": os.environ.get('CELERY_RESULT_BACKEND'),
        "beat_schedule": {
            "send-event-email": {
                'task': 'app.worker.email_worker.check_event_email_constantly',
                'schedule': 10,
            }
        }
    }


class DevelopmentConfig(Config):

    ENV = os.environ.get("FLASK_ENV", "development")
    DEBUG = True
    ASSETS_DEBUG = True


class TestingConfig(Config):

    ENV = os.environ.get("FLASK_ENV", "testing")
    DEBUG = True
    TESTING = True


class ProductionConfig(Config):

    ENV = os.environ.get("FLASK_ENV", "production")
    DEBUG = False
    USE_RELOADER = False


settings = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
}