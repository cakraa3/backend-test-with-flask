import pytest

from app import create_app
from app.database import db

@pytest.fixture()
def app():
    app, _ = create_app("sqlite://")

    with app.app_context():
        db.create_all()

    yield app

@pytest.fixture()
def client(app):
    return app.test_client()
