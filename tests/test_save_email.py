import pytest
from app.database import db
from app.database.models import Event, EventEmail
from app.worker import email_worker

import email.utils
import json

headers={
    'Content-type':'application/json', 
    'Accept':'application/json'
}

@pytest.fixture
def new_event(app):
    with app.app_context():
        event = Event(title='test', content='test')
        db.session.add(event)
        db.session.commit()
        db.session.refresh(event)
    return event

def test_save_email_success(client, app, new_event):
    response = client.post("/save_emails", headers=headers, json={
        "event_id": new_event.id,
        "email_subject": "Smile! \u263A",
        "email_content": "i'm lucky",
        "timestamp": "15 Dec 2015 23:12"
    })
    assert response.status_code == 201
    with app.app_context():
        eventEmail = EventEmail.query.first()
        timestamp = email.utils.parsedate_to_datetime("15 Dec 2015 23:12")
        assert eventEmail.event_id == new_event.id
        assert eventEmail.subject == "Smile! \u263A"
        assert eventEmail.content == "i'm lucky"
        assert eventEmail.timestamp == timestamp

def test_save_email_error(client, app, new_event):
    response = client.post("/save_emails", headers=headers, json={
        "event_id": 2,
        "email_subject": "Smile! \u263A",
        "email_content": "i'm lucky",
        "timestamp": "15 Dec 2015 23:12"
    })
    decoded_string = response.data.decode('utf-8')
    response_object = json.loads(decoded_string)
    assert response.status_code == 400
    assert response_object['message'] == 'Event id not found!'

# This test after the redis worker and beat started
def test_check_event_email_constantly_worker(app):
    with app.app_context():
        result = email_worker.check_event_email_constantly.delay()
        result.get(timeout=5)
        assert result.status == "SUCCESS"

